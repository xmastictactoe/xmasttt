var snowOn = true;
// nom et lien de l'image
var snowsrc = "snowflake.png"
// Nombre d'images
var no = 200;
// Durée de l'effet
var hidesnowtime = 0;
// "windowheight" or "pageheight"
var snowdistance = "pageheight";
var ie4up = (document.all) ? 2 : 2;
var ns6up = (document.getElementById && !document.all) ? 2 : 2;

function iecompattest() {
    return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document
        .body
}
var dx, xp, yp;
var am, stx, sty;
var i, doc_width = screen.width,
    doc_height = screen.height;
if (ns6up) {
    doc_width = self.innerWidth;
    doc_height = self.innerHeight;
} else if (ie4up) {
    doc_width = iecompattest().clientWidth;
    doc_height = iecompattest().clientHeight;
}
dx = new Array();
xp = new Array();
yp = new Array();
am = new Array();
stx = new Array();
sty = new Array();
for (i = 0; i < no; ++i) {
    dx[i] = 0;
    xp[i] = Math.random() * (doc_width - 50);
    yp[i] = Math.random() * doc_height;
    am[i] = Math.random() * 20;
    stx[i] = 0.02 + Math.random() / 10;
    sty[i] = 0.5 + Math.random();
    if (ie4up || ns6up) {
        if (i == 0) {
            document.write("<div id=\"dot" + i + "\" style=\"POSITION: absolute; Z-: " + i +
                "; VISIBILITY: visible; TOP: 15px; LEFT: 15px;\"><\/div>");
        } else {
            document.write("<div id=\"dot" + i + "\" style=\"POSITION: absolute; Z-: " + i +
                "; VISIBILITY: visible; TOP: 15px; LEFT: 15px;\"><img src='" + snowsrc +
                "' border=\"0\" height=\"15\"><\/div>");
        }
    }
}

function snowIE_NS6() { // IE and NS6 main animation function
    doc_width = ns6up ? window.innerWidth - 10 : iecompattest().clientWidth - 10;
    doc_height = (window.innerHeight && snowdistance == "windowheight") ? window.innerHeight : (ie4up &&
        snowdistance == "windowheight") ? iecompattest().clientHeight : (ie4up && !window.opera &&
        snowdistance == "pageheight") ? iecompattest().scrollHeight : iecompattest().offsetHeight;
    for (i = 0; i < no; ++i) {
        yp[i] += sty[i];
        if (yp[i] > doc_height - 50) {
            xp[i] = Math.random() * (doc_width - am[i] - 30);
            yp[i] = 0;
            stx[i] = 0.02 + Math.random() / 10;
            sty[i] = 0.5 + Math.random();
        }
        dx[i] += stx[i];
        document.getElementById("dot" + i).style.top = yp[i] + "px";
        document.getElementById("dot" + i).style.left = xp[i] + am[i] * Math.sin(dx[i]) + "px";
    }
    snowtimer = setTimeout("snowIE_NS6()", 10);
}

function hidesnow() {
    //if (window.snowtimer) clearTimeout(snowtimer)
    if (snowOn) {
        for (i = 0; i < no; i++) document.getElementById("dot" + i).style.display = "none"
        snowOn = false
    } else {
        console.log("Je suis caché")
        for (i = 0; i < no; i++) document.getElementById("dot" + i).style.display = "inline-block"
        snowOn = true
    }

}
if (ie4up || ns6up) {
    snowIE_NS6();
    if (hidesnowtime > 0)
        setTimeout("hidesnow()", hidesnowtime * 1000)
}

function restart() {
    //Paramétre taille de l'icone croix/rond, 128 coorespond à 128 pixels par un ratio, 
    //modifier selon taille en pixel de l'icone croix/rond. 
    var tailleImage = 128 * 1.01;

    // Recuperation du canvas
    var c = document.getElementById("canvasMorpion");
    var ctx = c.getContext("2d");

    // Recuperation tailles
    var largeur = c.width;
    var hauteur = c.height;

    // Choix taille grille
    var nbColonnes = 5;
    var nbLignes = 5;

    // Calcul taille cases
    var hauteurLigne = hauteur / nbLignes;
    var largeurColonne = largeur / nbColonnes;

    // Choix aspect croix
    var ratioCroix = 0.7;
    var epaisseurCroix = 1;
    var couleurCroix = "blue";

    // Choix aspect rond
    var ratioRond = 0.7;
    var epaisseurRond = 1;
    var couleurRond = "red";
    var rayonRond = largeurColonne;
    if (largeurColonne > hauteurLigne) {
        rayonRond = hauteurLigne;
    }
    rayonRond /= 2;
    rayonRond *= ratioRond;

    // Choix de la victoire
    var nbCoupsVictoire = 5;

    // Couleur de fond du canvas + contour
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    ctx.fillRect(0, 0, largeur, hauteur);
    ctx.strokeRect(0, 0, largeur, hauteur);

    // Initialisation du jeu
    var jeu = true;
    var joueurActuel = true;
    var coups = [];

    // Creation de la grille lignes*colonnes
    ctx.beginPath()
    ctx.lineWidth = 1;
    ctx.strokeStyle = "black";


    // Creation de la grille physique
    for (var i = 0; i < nbLignes - 1; i++) {
        // Creation ligne
        ctx.moveTo(0, (i + 1) * (hauteurLigne));
        ctx.lineTo(largeur, (i + 1) * (hauteurLigne));
        ctx.stroke();
    }
    for (var j = 0; j < nbColonnes - 1; j++) // Creation colonne
    {
        // Creation case (colonne)
        ctx.moveTo((j + 1) * (largeurColonne), 0);
        ctx.lineTo((j + 1) * (largeurColonne), hauteur);
        ctx.stroke();
    }

    ctx.closePath();

    // Evenement clic
    c.addEventListener("click", play, false);

    // Creation du tableau pour la grille
    for (var i = 0; i < nbLignes; i++) {
        for (var j = 0; j < nbColonnes; j++) {
            coups.push([]);
            coups[i].push(false);
        }
    }

    // Creation de croix
    function createCroix(x, y) {
        //Définir le coin d'apparition de l'icone croix
        startx = 0;
        starty = 0;
        base_image = new Image();
        base_image.src = 'img/croix2.png';
        base_image.onload = function () {
            if (x > 0 && x <= 1 * tailleImage) {
                startx = 0 * tailleImage;
            }
            if (x > 1 * tailleImage && x <= 2 * tailleImage) {
                startx = 1 * tailleImage;
            }
            if (x > 2 * tailleImage && x <= 3 * tailleImage) {
                startx = 2 * tailleImage;
            }
            if (x > 3 * tailleImage && x <= 4 * tailleImage) {
                startx = 3 * tailleImage;
            }
            if (x > 4 * tailleImage && x <= 5 * tailleImage) {
                startx = 4 * tailleImage;
            }
            if (x > 5 * tailleImage && x <= 6 * tailleImage) {
                startx = 5 * tailleImage;
            }

            if (y > 0 * tailleImage && y <= 1 * tailleImage) {
                starty = 0 * tailleImage;
            }
            if (y > 1 * tailleImage && y <= 2 * tailleImage) {
                starty = 1 * tailleImage;
            }
            if (y > 2 * tailleImage && y <= 3 * tailleImage) {
                starty = 2 * tailleImage;
            }
            if (y > 3 * tailleImage && y <= 4 * tailleImage) {
                starty = 3 * tailleImage;
            }
            if (y > 4 * tailleImage && y <= 5 * tailleImage) {
                starty = 4 * tailleImage;
            }
            if (y > 5 * tailleImage && y <= 6 * tailleImage) {
                starty = 5 * tailleImage;
            }
            ctx.drawImage(base_image, startx, starty);
        }

    }

    // Creation de rond
    function createRond(x, y) {
        // Définir le coin d'apparition de l'icone rond
        startx = 0;
        starty = 0;
        base_image = new Image();
        base_image.src = 'img/rond2.png';
        base_image.onload = function () {
            if (x > 0 && x <= 1 * tailleImage) {
                startx = 0 * tailleImage;
            }
            if (x > 1 * tailleImage && x <= 2 * tailleImage) {
                startx = 1 * tailleImage;
            }
            if (x > 2 * tailleImage && x <= 3 * tailleImage) {
                startx = 2 * tailleImage;
            }
            if (x > 3 * tailleImage && x <= 4 * tailleImage) {
                startx = 3 * tailleImage;
            }
            if (x > 4 * tailleImage && x <= 5 * tailleImage) {
                startx = 4 * tailleImage;
            }
            if (x > 5 * tailleImage && x <= 6 * tailleImage) {
                startx = 5 * tailleImage;
            }

            if (y > 0 && y <= 1 * tailleImage) {
                starty = 0 * tailleImage;
            }
            if (y > 1 * tailleImage && y <= 2 * tailleImage) {
                starty = 1 * tailleImage;
            }
            if (y > 2 * tailleImage && y <= 3 * tailleImage) {
                starty = 2 * tailleImage;
            }
            if (y > 3 * tailleImage && y <= 4 * tailleImage) {
                starty = 3 * tailleImage;
            }
            if (y > 4 * tailleImage && y <= 5 * tailleImage) {
                starty = 4 * tailleImage;
            }
            if (y > 5 * tailleImage && y <= 6 * tailleImage) {
                starty = 5 * tailleImage;
            }
            ctx.drawImage(base_image, startx, starty);
        }
    }

    // Verification fin
    function end() {
        for (var i = 0; i < nbLignes; i++) {
            for (var j = 0; j < nbColonnes; j++) {
                if (coups[i][j] == false) {
                    return false;
                }
            }
        }
        return true;
    }

    // Verification gagnant
    function gain(symbole, y, x) // on va chercher a verifier si il y a N symboles identiques alignés
    {
        var test = 0;
console.log("ligne");
        // Verification sur la meme ligne = sur le meme Y
        for (var i = 0; i < nbColonnes; i++) {
            console.log("i",i);
            console.log("symbole",symbole);
            console.log("(coups[y][i]",coups[y][i]);
            if (coups[y][i] == symbole) {
                test++;
                if (test >= nbCoupsVictoire) {
                    return true;
                }
            } else {
                test = 0;
            }
        }
console.log("colonne");
        test = 0;
        // Verification sur la meme colonne = sur le meme X
        for (var i = 0; i < nbLignes; i++) {
            console.log("i",i);
            console.log("symbole",symbole);
            console.log("(coups[i][x]",coups[i][x]);
            if (coups[i][x] == symbole) {
                test++;
                if (test >= nbCoupsVictoire) {
                    return true;
                }
            } else {
                test = 0;
            }
        }

        var x2 = x * 1;
        var y2 = y * 1;
        // Verification diagonale descendante
        while (x2 > 0 && y2 > 0) {
            x2--;
            y2--;
        }

        test = 0;
        while (x2 < nbColonnes && y2 < nbLignes) {
            if (coups[y2][x2] == symbole) {
                test++;
                if (test >= nbCoupsVictoire) {
                    return true;
                }
            } else {
                test = 0;
            }
            x2++;
            y2++;
        }

        x2 = x * 1;
        y2 = y * 1;
        // Verification diagonale asscendante
        while (x2 < nbColonnes - 1 && y2 > 0) {
            x2++;
            y2--;
        }

        test = 0;
        while (x2 >= 0 && y2 < nbLignes) {
            if (coups[y2][x2] == symbole) {
                test++;
                if (test >= nbCoupsVictoire) {
                    return true;
                }
            } else {
                test = 0;
            }
            x2--;
            y2++;
        }

        return false;
    }

    // Lorsqu on clique
    function play(event) {
        console.log(event);
        x = event.clientX - c.offsetLeft;
        y = event.clientY - c.offsetTop + document.documentElement.scrollTop;

        var caseX = parseInt(x / (largeur / nbColonnes));
        var caseY = parseInt(y / (hauteur / nbLignes));

        var milieuX = caseX * largeurColonne + largeurColonne / 2;
        var milieuY = caseY * hauteurLigne + hauteurLigne / 2;

        if (jeu) // Si jeu en route
        {
            if (!coups[caseY][caseX]) // Si pas déjà quelque chose sur la meme case
            {
                if (joueurActuel) {
                    createCroix(milieuX, milieuY);
                    coups[caseY][caseX] = "croix";
                    var temp = "croix";
                    document.getElementById("joueur").innerHTML = "Au joueur 2 de placer un rond";
                } else {
                    createRond(milieuX, milieuY);
                    coups[caseY][caseX] = "rond";
                    var temp = "rond";
                    document.getElementById("joueur").innerHTML = "Au joueur 1 de placer une croix";
                }

                joueurActuel = !joueurActuel;

                if (gain(temp, caseY, caseX)) {
                    if (joueurActuel) {
                        document.getElementById("joueur").innerHTML = "Victoire pour le joueur 2 !";
                        jeu = false;
                        document.getElementById("rejouer").style.display = "initial";
                    } else {
                        document.getElementById("joueur").innerHTML = "Victoire pour le joueur 1 !";
                        jeu = false;
                        document.getElementById("rejouer").style.display = "initial";
                    }
                } else {
                    if (end()) {
                        jeu = false;
                        document.getElementById("joueur").innerHTML = "Terminé. Personne n'a gagné.";
                        document.getElementById("rejouer").style.display = "visible";
                    }
                }
            }
        }

    }
}